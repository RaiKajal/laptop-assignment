package Com.crudNew.dao;


import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import Com.crudNew.dto.LaptopDTO;

public class HQL {
	Configuration con=null;
	SessionFactory factory=null;
	Session session=null;
	Transaction transaction=null;
	

	public  LaptopDTO read(int bar)
	{   con= new Configuration();
     	con.configure();
	   factory = con.buildSessionFactory();
	    session = factory.openSession();
		String hql="from LaptopDTO where bar=:bar";
		Query query = session.createQuery(hql);
		query.setParameter("bar",bar);
	    LaptopDTO result = (LaptopDTO) query.uniqueResult();
	    return result;

	}
     public void updatePrice(int bar,double price)
     { 
    	 con= new Configuration();
 		con.configure();
 		con.addAnnotatedClass(LaptopDTO.class);
 		factory = con.buildSessionFactory();
 		 session = factory.openSession();
 		 transaction = session.beginTransaction();
      String hql="update LaptopDTO set price=:price where bar=:bar";
      Query query = session.createQuery(hql);
      query.setParameter("bar",bar);
     query.setParameter("price",price);
     int update = query.executeUpdate();
     transaction.commit();
     if(update==0)
     {
     	System.out.println("Update failed");
     	return;
     }
     else
     {
     	System.out.println("Data updated successfully!");
     }
    	
     }
     public void updateStorage(int bar,String RAM,String Hd)
     {   con= new Configuration();
		con.configure();
		con.addAnnotatedClass(LaptopDTO.class);
		factory = con.buildSessionFactory();
		 session = factory.openSession();
		 transaction = session.beginTransaction();
    	 String hql="update LaptopDTO set RAM=:RAM ,hd=:Hd where bar=:bar";
         Query query = session.createQuery(hql);
         query.setParameter("bar",bar);
         query.setParameter("RAM",RAM);
        query.setParameter("Hd",Hd);
         int update = query.executeUpdate();
         transaction.commit();
         if(update==0)
         {
         	System.out.println("Update failed");
         	return;
         }
         else
         {
         	System.out.println("Data updated successfully!");
         }
        	 
       }
     public void delete(int bar)
     {   con= new Configuration();
		con.configure();
		factory = con.buildSessionFactory();
		 session = factory.openSession();
		 transaction = session.beginTransaction();
    	 String hql="delete LaptopDTO where bar=bar";//******CLASS NAME *******IN PLACE OF TABLE NAME******************//
  	    Query query = session.createQuery(hql);
  	   int update = query.executeUpdate();
  	  if(update!=0)
  		   System.out.println("Delete successfully");
  	   else
  	   System.out.println("failed deletion");
     }
     }
