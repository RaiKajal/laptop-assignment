package Com.crudNew.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import Com.crudNew.dto.LaptopDTO;


public class LaptopDAO {
	Configuration con=null;
	SessionFactory factory=null;
	Session session=null;
	Transaction transaction=null;

	  public boolean create(String RAM,String hd,double price,String company)
	     {
		  con= new Configuration();
			con.configure();
			factory = con.buildSessionFactory();
			 session = factory.openSession();
		  transaction = session.beginTransaction();
	    	LaptopDTO dto = new  LaptopDTO();
	    		 dto.setCompany(company);
	    		 dto.setRAM(RAM);
	    		 dto.setHd(hd);
	    		 dto.setPrice(price);
	    	     session.save(dto);
	    		 transaction.commit();
	    		 return true;
	     }
      public LaptopDTO read(int bar)
      {
    	  con= new Configuration();
  		con.configure();
  		factory = con.buildSessionFactory();
  		 session = factory.openSession();
    	return session.get(LaptopDTO.class,bar);
      }
     public boolean update(int bar,Double price)
     { 
    	 LaptopDTO dto=read(bar);
    	 if(dto!=null)
    	 {    con= new Configuration();
 		     con.configure();
 		   factory = con.buildSessionFactory();
 		    session = factory.openSession();
 		    transaction = session.beginTransaction();
    		 dto.setPrice(price);
    		 session.update(dto);
    		 transaction.commit();
    		 return true;
    	 }
    	 else
    		 return false;
     }
     public boolean delete(int bar)
     {
    	 LaptopDTO dto=read(bar);
    	 if(dto!=null)
    	 {     con= new Configuration();
 		      con.configure();
 		     factory = con.buildSessionFactory();
 		     session = factory.openSession();
 		      transaction = session.beginTransaction();
    		 transaction = session.beginTransaction();
    		 dto.setBar(bar);
    		 session.delete(dto);
    		 transaction.commit();
    		 return true;
    	 }
    	 else 
    		 return false;
     }
     
     
}
