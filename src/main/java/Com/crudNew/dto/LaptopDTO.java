package Com.crudNew.dto;
import java.io.Serializable;
import javax.persistence.*;
import org.hibernate.annotations.GenericGenerator;
@Entity
@Table(name="LaptopNew")
public class LaptopDTO implements Serializable {
  
@Id
   @GenericGenerator(name="code", strategy = "increment")
   @GeneratedValue(generator="code")
   private int bar;
   @Column(name="Company")
   private String company;
   @Column(name="RAM")
   private String RAM;
   @Column(name="Hard_Disk")
   private String hd;
   @Column(name="Price")
   private double price ;
public int getBar() {
	return bar;
}
public void setBar(int bar) {
	this.bar = bar;
}
public String getCompany() {
	return company;
}
public void setCompany(String company) {
	this.company = company;
}
public String getRAM() {
	return RAM;
}
public void setRAM(String rAM) {
	RAM = rAM;
}
public String getHd() {
	return hd;
}
public void setHd(String hd) {
	this.hd = hd;
}
public double getPrice() {
	return price;
}
public void setPrice(double price) {
	this.price = price;
}
public LaptopDTO() {
	super();
}
@Override
	public String toString() {
		return "LaptopDTO [bar=" + bar + ", company=" + company + ", RAM=" + RAM + ", hd=" + hd + ", price=" + price
				+ "]";
	}
}
